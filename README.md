# Anthony Goodier - Plum Guide Technical Test

To run the app please install the dependencies then run `yarn start`

## Improvements I didn't have time for

- Make the carousel more performant by only loading the necessary images and lazy loading in images when needed
- Improve carousel animation, just a basic slider with the time available
- General loading in animations
- Nicer transition when opening search
- Not using the default date picker
- Use a matching design set of SVGs so there arent any differences in sizing or stroke widths
