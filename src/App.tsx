import { useState } from 'react';
import { Header } from './components/header/Header';
import { Property } from './components/property/Property';
import { Search } from './components/search/Search';

function App() {

  const [showSearch, setShowSearch] = useState<boolean>(false);

  return (
    <main>
      <Header showSearch={showSearch} setShowSearch={setShowSearch} />
      <Search showSearch={showSearch} />
      <main>
        <Property />
      </main>
    </main>
  );
}

export default App;
