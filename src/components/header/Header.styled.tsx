import styled from 'styled-components'
import { BREAKPOINTS } from '../../utils/constants';

export const HeaderContainer = styled.header`
  border-bottom: 1px solid var(--brand-grey-light);
  display: grid;
  grid-template-columns: 8rem 1fr 8rem;
  height: 6rem;
  justify-content: stretch;

  @media (min-width: ${BREAKPOINTS.MOBILE}) {
    grid-template-columns: 8rem 1fr 1fr 1fr 8rem;
  }
`;

export const MenuButton = styled.button`
  border-right: 1px solid var(--brand-grey-light);
  display: flex;
  justify-content: center;
  align-items: center;
  width: 8rem;
  cursor: pointer;
  transition: background-color .2s;

  svg {
    height: 2rem;
  }

  &:hover, &:focus {
    background-color: var(--brand-secondary) ;
  }
`;

export const QuickLinks = styled.nav`
  display: flex;
  align-items: stretch;
  padding-left: 1rem;
  
  @media (max-width: ${BREAKPOINTS.MOBILE}) {
    display: none;
  }
`;

interface QuickLinkProps {
  isActive: boolean;
}

export const QuickLink = styled.a<QuickLinkProps>`
  display: block;
  padding: 0 var(--gutter);
  line-height: 6rem;
  position: relative;
  letter-spacing: 1px;
  color: ${props => props.isActive ? 'var(--brand-text)' : 'var(--brand-text-light)'};
  text-decoration: none;
  margin: 0 1rem;
  font-size: 1.4rem;

  &::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    height: .2rem;
    background-color: ${props => props.isActive ? 'var(--brand-secondary)' : 'transparent'};
    transition: background-color .2s;
  }

  &:hover, &:focus {
    color: var(--brand-text);

    &::after {
      background-color: ${props => props.isActive ? 'var(--brand-secondary)' : 'var(--brand-grey)'};
    }
  }
`;

export const Logo = styled.a`
  text-decoration: none;
  color: var(--brand-text);
  font-size: 3rem;
  text-align: center;
  align-self: center;
  transition: color .2s;
  cursor: pointer;
  font-family: var(--title-font);

  svg {
    height: 2rem;
  }

  &:hover, &:focus {
    color: var(--brand-secondary);
  }
`;

export const UserLinks = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  @media (max-width: ${BREAKPOINTS.MOBILE}) {
    display: none;
  }
`;

export const UserLink = styled.button`
  text-decoration: none;
  color: var(--brand-text-light);
  letter-spacing: 1px;
  cursor: pointer;
  margin: 0 var(--gutter);
  font-size: 1.2rem;

  &:hover, &:focus {
    text-decoration: underline;
    color: var(--brand-text);
  }
`;

export const SearchButton = styled.button`
  border-left: 1px solid var(--brand-grey-light);
  display: flex;
  justify-content: center;
  align-items: center;
  width: 8rem;
  justify-self: flex-end;
  cursor: pointer;
  transition: background-color .2s;

  svg {
    height: 2rem;
  }

  &:hover, &:focus {
    background-color: var(--brand-secondary) ;
  }
`;