import { Dispatch, SetStateAction } from "react"
import { Menu, PlumGuideLogo, Search } from "../shared/SVGs"
import { HeaderContainer, Logo, MenuButton, QuickLink, QuickLinks, SearchButton, UserLink, UserLinks } from "./Header.styled"

interface Props {
  showSearch: boolean;
  setShowSearch: Dispatch<SetStateAction<boolean>>;
}

export const Header = ({showSearch, setShowSearch}: Props) => {
  return (
    <HeaderContainer>
      <MenuButton>
        <Menu />
      </MenuButton>
      <QuickLinks>
        <QuickLink isActive={true} href="#">Homes</QuickLink>
        <QuickLink isActive={false} href="#">Hosts</QuickLink>
      </QuickLinks>
      <Logo href="/">
        <PlumGuideLogo />
      </Logo>
      <UserLinks>
        <UserLink>Need help?</UserLink>
        <UserLink>Login</UserLink>
      </UserLinks>
      <SearchButton onClick={() => setShowSearch(!showSearch)}>
        <Search />
      </SearchButton>
    </HeaderContainer>
  )
}
