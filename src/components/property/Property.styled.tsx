import styled from "styled-components";
import { BREAKPOINTS } from "../../utils/constants";

export const PropertyContainer = styled.section`
  background: var(--brand-primary);
  
  @media (min-width: ${BREAKPOINTS.TABLET}) {
    padding-bottom: 5rem;
    background: linear-gradient(var(--brand-primary) 90%, white 10%);
  }
`;