import { PropertyHeader } from './PropertyHeader'
import { PropertyContainer } from './Property.styled'
import { PropertyGallery } from './PropertyGallery'

export const Property = () => {
  return (
    <PropertyContainer>
      <PropertyHeader />
      <PropertyGallery />
    </PropertyContainer>
  )
}
