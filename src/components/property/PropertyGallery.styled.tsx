import styled from "styled-components";
import { BREAKPOINTS } from "../../utils/constants";

export const PropertGalleryContainer = styled.div`
  position: relative;
  
  @media (min-width: ${BREAKPOINTS.DESKTOP}) {
    max-width: 120rem;
    margin: 0 auto;
  }
`;

export const PropertyGalleryNav = styled.nav`
  display: flex;
  justify-content: flex-end;
`;

interface PropertyGalleryNavButtonProps {
  readonly position: string;
}

export const PropertyGalleryNavButton = styled.button<PropertyGalleryNavButtonProps>`
  height: 4rem;
  width: 4rem;
  text-align: center;
  transition: color .2s;
  cursor: pointer;

  &:hover, &:focus {
    color: white;
  }

  svg {
    width: 3rem;
    height: 4rem;
  }
  
  @media (min-width: ${BREAKPOINTS.DESKTOP}) {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: ${props => props.position === 'left' ? '-4rem' : 'auto'};
    right: ${props => props.position === 'right' ? '-4rem' : 'auto'};
    z-index: 10;
    height: 6rem;
    width: 6rem;

    svg {
      width: 100%;
      height: 100%;
    }
  }
`;

export const PropertyGalleryCount = styled.div`
  position: absolute;
  top: 50%;
  right: 3rem;
  transform: translateY(-50%);
  background: white;
  width: 3.6rem;
  height: 3.6rem;
  line-height: 3.6rem;
  text-align: center;
  border-radius: 100%;
  z-indeX: 10;
  font-size: 1.1rem;
  
  @media (max-width: ${BREAKPOINTS.DESKTOP}) {
    display: none;
  }
`;

export const PropertGalleryCarouselWrapper = styled.div`
  overflow: hidden;
`;

export const PropertGalleryCarousel = styled.ul`
  width: 100%;
  white-space: nowrap;
  font-size: 0; // Removes inline-block mystery padding
  transform: translateX(var(--carousel-position));
  transition: transform .2s ease-out;
`;

export const PropertGalleryItem= styled.li`
  display: inline-block;
  width: 100%;

  img {
    width: 100%;
    display: block;
  }
`;