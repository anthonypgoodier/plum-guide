import { useState, useEffect } from 'react'
import { ArrowLeft, ArrowRight } from '../shared/SVGs';
import { PropertGalleryCarousel, PropertGalleryCarouselWrapper, PropertGalleryContainer, PropertGalleryItem, PropertyGalleryCount, PropertyGalleryNav, PropertyGalleryNavButton } from './PropertyGallery.styled';
import { PropertyGalleryImage } from './PropertyGalleryImage';

const MAX_PHOTOS = 30;

export const PropertyGallery = () => {

  const [photos, setPhotos] = useState<string[]>([]);
  const [carouselPosition, setCarouselPosition] = useState<number>(0);

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch('https://run.mocky.io/v3/8dac4388-ce28-4406-95bb-91aec813168d');
      const data = await res.json();
      const topPhotos = data.imageUrls.slice(0, MAX_PHOTOS); // Couldn't see anyway to limit the number of images via the API
      setPhotos(topPhotos);
    }
    fetchData();
  }, [])

  const handleCarousel = (reverse: boolean) => reverse ? setCarouselPosition(carouselPosition - 1) : setCarouselPosition(carouselPosition + 1);

  if ( photos.length === 0 ) return null;

  return (
    <PropertGalleryContainer>
      <PropertyGalleryNav>
        <PropertyGalleryNavButton position="left" onClick={() => handleCarousel(true)} disabled={carouselPosition === 0}>
          <ArrowLeft />
        </PropertyGalleryNavButton>
        <PropertyGalleryNavButton position="right" onClick={() => handleCarousel(false)} disabled={carouselPosition === MAX_PHOTOS - 1}>
          <ArrowRight />
        </PropertyGalleryNavButton>
      </PropertyGalleryNav>
      <PropertyGalleryCount>{ carouselPosition + 1 }/{ MAX_PHOTOS }</PropertyGalleryCount>
      <PropertGalleryCarouselWrapper>
        <PropertGalleryCarousel style={{ transform: `translateX(${carouselPosition * -100}%)`}}>
          { photos.map( (photo, index) => (
            <PropertGalleryItem key={index}>
              <PropertyGalleryImage photo={photo} />
            </PropertGalleryItem>
          ))}
        </PropertGalleryCarousel>
      </PropertGalleryCarouselWrapper>
    </PropertGalleryContainer>
  )
}
