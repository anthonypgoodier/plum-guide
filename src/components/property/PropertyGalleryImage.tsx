interface Props {
  photo: string;
}

export const PropertyGalleryImage = ({photo}: Props) => {
  return (
    <picture>
      <source media="(max-width: 400px)" srcSet={`${photo}?w=400&dpr=1 1x, ${photo}?w=400&dpr=2 2x`} />
      <source media="(max-width: 800px)" srcSet={`${photo}?w=800&dpr=1 1x, ${photo}?w=800&dpr=2 2x`} />
      <source media="(min-width: 801px)" srcSet={`${photo}?w=1200&dpr=1 1x, ${photo}?w=1200&dpr=2 2x`} />
      <img src={`${photo}`} alt="" />
    </picture>
  )
}
