import styled from "styled-components";
import { BREAKPOINTS } from "../../utils/constants";

export const PropertyHeaderContainer = styled.header`
  padding: var(--gutter-large) var(--gutter-large) var(--gutter);
  color: var(-text-light);
`;

export const PropertyTitle = styled.h2`
  text-align: center;
  font-family: var(--title-font);
  font-size: 4rem;
  margin-bottom: var(--gutter);
`;

export const PropertyAmenities = styled.ul`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  text-align: center;
  margin-bottom: var(--gutter);
`;

export const PropertyAmenity = styled.li`
  padding: 0 1rem;
  white-space: nowrap;
  line-height: 3rem;
`;

export const LocationInfoWrapper = styled.ul`
  
  @media (min-width: ${BREAKPOINTS.MOBILE}) {
    display: flex;
    justify-content: center;
  }
`;

export const LocationInfo = styled.ul`
  padding: 0 1rem;
  border-top: 1px solid rgba(0,0,0,0.2);
  border-bottom: 1px solid rgba(0,0,0,0.2);
  
  @media (min-width: ${BREAKPOINTS.MOBILE}) {
    padding: 1rem 0;
    display: flex;
    justify-content: center;
  }
`;

export const LocationDetail = styled.li`
  text-align: center;
  padding: 1.4rem 0;
  white-space: nowrap;

  svg {
    vertical-align: middle;
    margin-right: .4rem;
    width: 2rem;
    height: 2rem
  }
  
  @media (min-width: ${BREAKPOINTS.MOBILE}) {
    padding: .6rem var(--gutter);

    &:not(:last-child) {
      border-right: 1px solid rgba(0,0,0,0.2);
    }
  }

  @media (max-width: ${BREAKPOINTS.MOBILE}) {

    &:not(:last-child) {
      border-bottom: 1px solid rgba(0,0,0,0.2);
    }
  }
`;

