import { Location, Stairs, Train } from '../shared/SVGs'
import { LocationDetail, LocationInfo, LocationInfoWrapper, PropertyAmenities, PropertyAmenity, PropertyHeaderContainer, PropertyTitle } from './PropertyHeader.styled'

interface Props {
  
}

export const PropertyHeader = (props: Props) => {
  return (
    <PropertyHeaderContainer>
      <PropertyTitle>Monsieur Didot</PropertyTitle>
      <PropertyAmenities>
        <PropertyAmenity>4 people</PropertyAmenity>
        <PropertyAmenity>2 bedrooms</PropertyAmenity>
        <PropertyAmenity>2 bathrooms</PropertyAmenity>
        <PropertyAmenity>Private terrasse</PropertyAmenity>
        <PropertyAmenity>Peaceful</PropertyAmenity>
      </PropertyAmenities>
      <LocationInfoWrapper>
        <LocationInfo>
          <LocationDetail>
            <Location />
            Notting Hill, London
          </LocationDetail>
          <LocationDetail>
            <Train />
            Walk 6 mins (Westbourne Park Station
          </LocationDetail>
          <LocationDetail>
            <Stairs />
            Stairs
          </LocationDetail>
        </LocationInfo>
      </LocationInfoWrapper>
    </PropertyHeaderContainer>
  )
}
