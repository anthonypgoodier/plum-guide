import styled from "styled-components";
import { BREAKPOINTS } from '../../utils/constants';

export const SearchContainer = styled.section`
  padding: var(--gutter);
  display: grid;
  grid-gap: var(--gutter);
  
  @media (min-width: ${BREAKPOINTS.DESKTOP}) {
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    max-width: var(--wrapper);
    margin: 0 auto;
    padding: var(--gutter-large);
  }
`;

export const SearchField = styled.div`
  
  @media (max-width: ${BREAKPOINTS.DESKTOP}) {
  border-bottom: 1px solid var(--brand-grey-light);
  padding-bottom: var(--gutter);
  }
  
  @media (min-width: ${BREAKPOINTS.DESKTOP}) {
    
    &:not(:last-of-type) {
      padding-right: var(--gutter);
      border-right: 1px solid var(--brand-grey-light);
    }
  }
`;

export const TextLabel = styled.div`
  font-size: 1.4rem;
  margin-bottom: 1rem;
  color: var(--brand-text-light);
`;

export const TextValue = styled.div`
  font-family: var(--body-font-bold);
`;

export const InputContainer = styled.div`
  position: relative;
  
  svg {
    position: absolute;
    top: 50%;
    right: 0;
    height: 2rem;
    width: 2rem;
    transform: translateY(-50%) rotate(90deg);
  }
`;

export const Input = styled.input`
  display: block;
  width: 100%;
  margin: 0 auto;
  height: 2rem;
  border: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  font-family: var(--body-font-bold);

  &::-webkit-calendar-picker-indicator {
    color: transparent;
    background: none;
    z-index: 1;
  }
`;