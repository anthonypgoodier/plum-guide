import { CallToAction } from '../shared/CallToAction'
import { Chevron } from '../shared/SVGs'
import { SearchContainer, Input, TextLabel, SearchField, InputContainer, TextValue } from './Search.styled'

interface Props {
  showSearch: boolean;
}

export const Search = ({showSearch}: Props) => {

  if ( !showSearch ) return null;
  
  return (
    <SearchContainer>
      <SearchField>
        <TextLabel>From/To</TextLabel>
        <InputContainer>
          <Input type="date" />
          <Chevron />
        </InputContainer>
      </SearchField>
      <SearchField>
        <label>
          <TextLabel>For</TextLabel>
          <InputContainer>
            <Input as="select">
              <option>1 Guest</option>
              <option>2 Guests</option>
              <option>3 Guests</option>
              <option>4 Guests</option>
            </Input>
            <Chevron />
          </InputContainer>
        </label>
      </SearchField>
      <SearchField>
        <div>
          <TextLabel>£ Per night</TextLabel>
          <TextValue>345</TextValue>
        </div>
      </SearchField>
      <SearchField>
        <div>
          <TextLabel>£ Total for 54 nights</TextLabel>
          <TextValue>18,360</TextValue>
        </div>
      </SearchField>
      <CallToAction label="Instant Booking" />
    </SearchContainer>
  )
}
