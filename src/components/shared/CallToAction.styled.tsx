import styled from "styled-components";

export const Button = styled.button`
  background-color: var(--brand-secondary);
  line-height: 5rem;
  padding: 0 3rem;
  text-transform: uppercase;
  transition: background-color .2s;
  letter-spacing: .1rem;
  cursor: pointer;

  &:hover, &:focus {
    background-color: var(--brand-primary);
  }
`;