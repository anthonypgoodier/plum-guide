import { Button } from './CallToAction.styled'

interface Props {
  label: string;
}

export const CallToAction = ({label}: Props) => <Button>{label}</Button>;
