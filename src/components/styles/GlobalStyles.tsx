import { createGlobalStyle } from 'styled-components'
import reset from 'styled-reset'

export const GlobalStyles = createGlobalStyle`
  ${reset}

  @font-face {
    font-family: 'HalyardDis';
    src: url(./fonts/HalyardDisplay-Regular.woff2) format('woff2');
  }

  @font-face {
    font-family: 'HalyardDisBook';
    src: url(./fonts/HalyardDisBook.woff2) format('woff2');
  }

  @font-face {
    font-family: 'DomaineNarrowMedium';
    src: url(./fonts/DomaineDisplayNarrowWeb-Medium.woff2) format('woff2');
  }

  :root {
    --brand-grey: #636366;
    --brand-grey-light: #ececec;
    --brand-primary: #f7ddd1;
    --brand-secondary: #fdbb30;
    --brand-text: #1d1d1d;
    --brand-text-light: #606060;

    --body-font: "HalyardDisBook", Arial, "Helvetica Neue", Helvetica, sans-serif;
    --body-font-bold: "HalyardDis", Arial, "Helvetica Neue", Helvetica, sans-serif;
    --title-font: "DomaineNarrowMedium", Arial, "Helvetica Neue", Helvetica, sans-serif;

    --gutter: 2rem;
    --gutter-large: 4rem;
    --wrapper: 120rem;
  }

  * {
    box-sizing: border-box;
  }

  html {
    font-size: 62.5%;
  }

  body {
    background: white;
    font-family: var(--body-font);
    font-size: 1.6rem;
  }

  button {
    background: none;
    border: none;
    padding: 0;
  }
`