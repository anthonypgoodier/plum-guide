export const BREAKPOINTS = {
  MOBILE: '800px',
  TABLET: '1100px',
  DESKTOP: '1300px'
}